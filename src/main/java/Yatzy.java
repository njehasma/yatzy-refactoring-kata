import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Yatzy {

    public static final int SCORE_FIFTY = 50;
    public static final int SCORE_ZERO = 0;
    public static final int SMALL_STRAIGHT_SCORE = 15;
    public static final int LARGE_STRAIGHT_SCORE = 20;
    public static final int TWO_OCCURRENCES = 2;
    public static final int THREE_OCCURRENCES = 3;
    public static final int FOUR_OCCURRENCES = 4;
    public static final int FIVE_OCCURRENCES = 5;
    public static final int LAST_INDEX = 5;
    public static final String EQUALS_OPERATION = "equals";
    public static final String GREATER_THAN_OR_EQUALS_TO_OPERATION = "greaterThanOrEqualTo";

    public List<Dice> dice = new ArrayList<>(5);

    public static int chance(List<Dice> diceList) {
        if (!CollectionUtils.isEmpty(diceList)) {
            return diceList.stream()
                    .mapToInt(Dice::value)
                    .sum();
        }
        return SCORE_ZERO;
    }

    public static int yatzy(List<Dice> diceList) {
        if (Boolean.TRUE.equals(checkOccurrencesAndGetIndex(diceList, FIVE_OCCURRENCES, EQUALS_OPERATION).getKey())) {
            return SCORE_FIFTY;
        }
        return SCORE_ZERO;
    }

    public static int ones(List<Dice> diceList) {
        return sumOfDiceValuesByDice(diceList, Dice.ONE.value());
    }

    public static int twos(List<Dice> diceList) {
        return sumOfDiceValuesByDice(diceList, Dice.TWO.value());
    }

    public static int threes(List<Dice> diceList) {
        return sumOfDiceValuesByDice(diceList, Dice.THREE.value());
    }

    public int fours() {
        return sumOfDiceValuesByDice(dice, Dice.FOUR.value());
    }

    public int fives() {
        return sumOfDiceValuesByDice(dice, Dice.FIVE.value());
    }

    public int sixes() {
        return sumOfDiceValuesByDice(dice, Dice.SIX.value());
    }

    public Yatzy(List<Dice> diceList) {
        dice.addAll(diceList);
    }

    public static int Pair(List<Dice> diceList) {
        if (!CollectionUtils.isEmpty(diceList)) {
            return getPair(diceList)
                    .findFirst()
                    .orElse(SCORE_ZERO);
        }
        return SCORE_ZERO;
    }

    public static int twoPair(List<Dice> diceList) {
        if (!CollectionUtils.isEmpty(diceList)) {
            if (getPair(diceList).count() == TWO_OCCURRENCES) {
                return getPair(diceList)
                        .sum();
            }
        }
        return SCORE_ZERO;
    }

    public static int fourOfAKind(List<Dice> diceList) {
        Pair<Boolean, Integer> fourOccIndex = checkOccurrencesAndGetIndex(diceList, FOUR_OCCURRENCES, GREATER_THAN_OR_EQUALS_TO_OPERATION);
        if (Boolean.TRUE.equals(fourOccIndex.getKey())) {
            return (fourOccIndex.getValue() + 1) * FOUR_OCCURRENCES;
        }
        return SCORE_ZERO;
    }

    public static int threeOfAKind(List<Dice> diceList) {
        Pair<Boolean, Integer> threeOccIndex = checkOccurrencesAndGetIndex(diceList, THREE_OCCURRENCES, GREATER_THAN_OR_EQUALS_TO_OPERATION);
        if (Boolean.TRUE.equals(threeOccIndex.getKey())) {
            return (threeOccIndex.getValue() + 1) * THREE_OCCURRENCES;
        }
        return SCORE_ZERO;
    }

    public static int smallStraight(List<Dice> diceList) {
        return Boolean.TRUE.equals(isItAllDicesEqualsOne(diceList, 0)) ? SMALL_STRAIGHT_SCORE : SCORE_ZERO;
    }

    public static int largeStraight(List<Dice> diceList) {
        return Boolean.TRUE.equals(isItAllDicesEqualsOne(diceList, 1)) ? LARGE_STRAIGHT_SCORE : SCORE_ZERO;
    }

    public static int fullHouse(List<Dice> diceList) {
        Pair<Boolean, Integer> twoOccIndex = checkOccurrencesAndGetIndex(diceList, TWO_OCCURRENCES, EQUALS_OPERATION);
        Pair<Boolean, Integer> threeOccIndex = checkOccurrencesAndGetIndex(diceList, THREE_OCCURRENCES, EQUALS_OPERATION);
        if (Boolean.TRUE.equals(twoOccIndex.getKey())
                && Boolean.TRUE.equals(threeOccIndex.getKey())) {
            return (twoOccIndex.getValue() + 1) * TWO_OCCURRENCES + (threeOccIndex.getValue() + 1) * THREE_OCCURRENCES;
        }
        return SCORE_ZERO;
    }

    private static Pair<Boolean, Integer> checkOccurrencesAndGetIndex(List<Dice> diceList, int nbOccurrences, String operation) {
        if (!CollectionUtils.isEmpty(diceList)) {
            final int[] counts = new int[6];
            for (Dice dice : diceList) {
                counts[dice.value() - 1]++;
            }
            return IntStream.range(0, counts.length)
                    .filter(i -> operation.equals(EQUALS_OPERATION) ?
                            counts[i] == nbOccurrences :
                            counts[i] >= nbOccurrences)
                    .mapToObj(i -> Pair.of(Boolean.TRUE, i))
                    .findFirst()
                    .orElse(Pair.of(Boolean.FALSE, SCORE_ZERO));
        }
        return Pair.of(Boolean.FALSE, SCORE_ZERO);
    }

    private static int sumOfDiceValuesByDice(List<Dice> diceList, int value) {
        if (!CollectionUtils.isEmpty(diceList)) {
            return diceList.stream()
                    .filter(dice -> dice.value() == value)
                    .mapToInt(Dice::value)
                    .sum();
        }
        return SCORE_ZERO;
    }

    private static Boolean isItAllDicesEqualsOne(List<Dice> diceList, int firstIndex) {
        if (!CollectionUtils.isEmpty(diceList)) {
            final int[] counts = new int[6];
            for (Dice dice : diceList) {
                counts[dice.value() - 1]++;
            }
            return IntStream.range(firstIndex, firstIndex + 4)
                    .allMatch(i -> counts[i] == 1);
        }
        return false;
    }

    private static IntStream getPair(List<Dice> diceList) {
        final int[] counts = new int[6];
        for (Dice dice : diceList) {
            counts[dice.value() - 1]++;
        }
        return IntStream.range(0, counts.length)
                .filter(i -> counts[LAST_INDEX - i] >= TWO_OCCURRENCES)
                .map(i -> (6 - i) * TWO_OCCURRENCES);
    }
}



