import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class YatzyTest {

    @Test
    public void chance() {
        assertEquals(15, Yatzy.chance(Arrays.asList(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.ONE)));
        assertEquals(16, Yatzy.chance(Arrays.asList(Dice.THREE, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.ONE)));
    }

    @Test
    public void yatzy() {
        assertEquals(Yatzy.SCORE_FIFTY, Yatzy.yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FOUR, Dice.FOUR, Dice.FOUR)));
        assertEquals(Yatzy.SCORE_FIFTY, Yatzy.yatzy(Arrays.asList(Dice.SIX, Dice.SIX, Dice.SIX, Dice.SIX, Dice.SIX)));
        assertEquals(Yatzy.SCORE_ZERO, Yatzy.yatzy(Arrays.asList(Dice.SIX, Dice.SIX, Dice.SIX, Dice.SIX, Dice.THREE)));
    }

    @Test
    public void ones() {
        assertEquals(1, Yatzy.ones(Arrays.asList(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE)));
        assertEquals(2, Yatzy.ones(Arrays.asList(Dice.ONE, Dice.TWO, Dice.ONE, Dice.FOUR, Dice.FIVE)));
        assertEquals(Yatzy.SCORE_ZERO, Yatzy.ones(Arrays.asList(Dice.SIX, Dice.TWO, Dice.TWO, Dice.FOUR, Dice.FIVE)));
        assertEquals(4, Yatzy.ones(Arrays.asList(Dice.ONE, Dice.TWO, Dice.ONE, Dice.ONE, Dice.ONE)));
    }

    @Test
    public void twos() {
        assertEquals(4, Yatzy.twos(Arrays.asList(Dice.ONE, Dice.TWO, Dice.THREE, Dice.TWO, Dice.SIX)));
        assertEquals(10, Yatzy.twos(Arrays.asList(Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO, Dice.TWO)));
    }

    @Test
    public void threes() {
        assertEquals(6, Yatzy.threes(Arrays.asList(Dice.ONE, Dice.TWO, Dice.THREE, Dice.TWO, Dice.THREE)));
        assertEquals(12, Yatzy.threes(Arrays.asList(Dice.TWO, Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE)));
    }

    @Test
    public void fours() {
        assertEquals(12, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FOUR, Dice.FIVE, Dice.FIVE)).fours());
        assertEquals(8, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FIVE, Dice.FIVE, Dice.FIVE)).fours());
        assertEquals(4, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FIVE, Dice.FIVE, Dice.FIVE, Dice.FIVE)).fours());
    }

    @Test
    public void fives() {
        assertEquals(10, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FOUR, Dice.FIVE, Dice.FIVE)).fives());
        assertEquals(15, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FIVE, Dice.FIVE, Dice.FIVE)).fives());
        assertEquals(20, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FIVE, Dice.FIVE, Dice.FIVE, Dice.FIVE)).fives());
    }

    @Test
    public void sixes() {
        assertEquals(Yatzy.SCORE_ZERO, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.FOUR, Dice.FIVE, Dice.FIVE)).sixes());
        assertEquals(6, new Yatzy(Arrays.asList(Dice.FOUR, Dice.FOUR, Dice.SIX, Dice.FIVE, Dice.FIVE)).sixes());
        assertEquals(18, new Yatzy(Arrays.asList(Dice.SIX, Dice.FIVE, Dice.SIX, Dice.SIX, Dice.FIVE)).sixes());
    }

    @Test
    public void onePair() {
        assertEquals(6, Yatzy.Pair(Arrays.asList(Dice.THREE, Dice.FOUR, Dice.THREE, Dice.FIVE, Dice.SIX)));
        assertEquals(10, Yatzy.Pair(Arrays.asList(Dice.FIVE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.FIVE)));
        assertEquals(12, Yatzy.Pair(Arrays.asList(Dice.FIVE, Dice.THREE, Dice.SIX, Dice.SIX, Dice.FIVE)));
    }

    @Test
    public void twoPair() {
        assertEquals(16, Yatzy.twoPair(Arrays.asList(Dice.THREE, Dice.THREE, Dice.FIVE, Dice.FOUR, Dice.FIVE)));
        assertEquals(16, Yatzy.twoPair(Arrays.asList(Dice.THREE, Dice.THREE, Dice.FIVE, Dice.FIVE, Dice.FIVE)));
    }

    @Test
    public void threeOfAKind() {
        assertEquals(9, Yatzy.threeOfAKind(Arrays.asList(Dice.THREE, Dice.THREE, Dice.THREE, Dice.FOUR, Dice.FIVE)));
        assertEquals(15, Yatzy.threeOfAKind(Arrays.asList(Dice.FIVE, Dice.THREE, Dice.FIVE, Dice.FOUR, Dice.FIVE)));
        assertEquals(9, Yatzy.threeOfAKind(Arrays.asList(Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.FIVE)));
    }

    @Test
    public void fourOfAKind() {
        assertEquals(12, Yatzy.fourOfAKind(Arrays.asList(Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.FIVE)));
        assertEquals(20, Yatzy.fourOfAKind(Arrays.asList(Dice.FIVE, Dice.FIVE, Dice.FIVE, Dice.FOUR, Dice.FIVE)));
        assertEquals(9, Yatzy.threeOfAKind(Arrays.asList(Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE, Dice.THREE)));
    }

    @Test
    public void smallStraight() {
        assertEquals(Yatzy.SMALL_STRAIGHT_SCORE, Yatzy.smallStraight(Arrays.asList(Dice.ONE, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE)));
        assertEquals(Yatzy.SMALL_STRAIGHT_SCORE, Yatzy.smallStraight(Arrays.asList(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.ONE)));
        assertEquals(Yatzy.SCORE_ZERO, Yatzy.smallStraight(Arrays.asList(Dice.ONE, Dice.TWO, Dice.TWO, Dice.FOUR, Dice.FIVE)));
    }

    @Test
    public void largeStraight() {
        assertEquals(Yatzy.LARGE_STRAIGHT_SCORE, Yatzy.largeStraight(Arrays.asList(Dice.SIX, Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE)));
        assertEquals(Yatzy.LARGE_STRAIGHT_SCORE, Yatzy.largeStraight(Arrays.asList(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.SIX)));
        assertEquals(Yatzy.SCORE_ZERO, Yatzy.largeStraight(Arrays.asList(Dice.ONE, Dice.TWO, Dice.TWO, Dice.FOUR, Dice.FIVE)));
    }

    @Test
    public void fullHouse() {
        assertEquals(18, Yatzy.fullHouse(Arrays.asList(Dice.SIX, Dice.TWO, Dice.TWO, Dice.TWO, Dice.SIX)));
        assertEquals(Yatzy.SCORE_ZERO, Yatzy.fullHouse(Arrays.asList(Dice.TWO, Dice.THREE, Dice.FOUR, Dice.FIVE, Dice.SIX)));
    }
}
